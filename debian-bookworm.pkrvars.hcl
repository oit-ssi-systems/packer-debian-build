template-os-version  = "debian-12.9.0-amd64"
installer-iso-url    = "https://archive.linux.duke.edu/debian-cdimage/12.9.0/amd64/iso-cd/debian-12.9.0-amd64-netinst.iso"
installer-iso-sha256-url = "https://archive.linux.duke.edu/debian-cdimage/12.9.0/amd64/iso-cd/SHA256SUMS"